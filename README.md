
# universe-modpack
**Клиентский модпак сервера Universe**

**[Скачать](https://gitlab.com/universemcapp/universe-modpack/-/releases) ⬇️**

**Версия:** Fabric 1.19.2
**Fabric Loader:** 0.14.9 или выше
https://fabricmc.net/use/installer

## Ресурс паки:
- <img src="https://media.forgecdn.net/avatars/thumbnails/452/679/64/64/637717126710811809.png" alt="drawing" width="28"/> [BvBOverlays](https://www.curseforge.com/minecraft/texture-packs/bvb-overlays) - создает соединения блоков окружающей среды
- <img src="https://media.forgecdn.net/avatars/thumbnails/378/78/64/64/637556725706865300.png" alt="drawing" width="28"/> [CTM_Overhaul](https://www.curseforge.com/minecraft/texture-packs/connected-textures-mod-ctm-overhaul) - соединяет некоторые текстуры
- <img src="https://media.forgecdn.net/avatars/thumbnails/549/5/64/64/637885620520159814.png" alt="drawing" width="28"/> [Icons+](https://www.curseforge.com/minecraft/texture-packs/icons) - добавляет иконки в меню и зачарованиях
- <img src="https://media.forgecdn.net/avatars/thumbnails/191/909/64/64/636862047595504286.png" alt="drawing" width="28"/> [JappaGlass](https://www.curseforge.com/minecraft/texture-packs/new-glass) - делает стекло соединенным
- <img src="https://universemc.ru/assets/logo.png" alt="drawing" width="28"/> [UniverseTotems](https://drive.google.com/drive/folders/17QB_TmjJcwy0uuPGjHd8BSvydQcY_N7f) - тотемы со скинами, автор Reamyr
- <img src="https://universemc.ru/assets/logo.png" alt="drawing" width="28"/> [U_RP](https://vk.com/topic-191976963_47070782) - РП-Пак, автор Mr_JSPA, KTochka

## Модификации:
### Дополнения сетевой игры:
- <img src="https://cdn.modrinth.com/data/pZ2wrerK/icon.png" alt="drawing" width="28"/> [Emotecraft](https://modrinth.com/mod/emotecraft) - создавайте свои собственные эмоции в Minecraft
- <img src="https://cdn.modrinth.com/data/1bZhdhsH/icon.png" alt="drawing" width="28"/> [PlasmoVoice](https://modrinth.com/mod/plasmo-voice) - голосовой чат для сервера (смотрите настройки управления)

### Украшения:
- <img src="https://cdn.modrinth.com/data/zV5r3pPn/icon.png" alt="drawing" width="28"/> [3DSkinLayers](https://modrinth.com/mod/3dskinlayers) - 3D скины
- <img src="https://cdn.modrinth.com//data/rUgZvGzi/icon.gif" alt="drawing" width="28"/> [EatingAnimation](https://modrinth.com/mod/eating-animation) - добавляет анимацию спрайтов для съедобных и питьевых предметов
- <img src="https://media.forgecdn.net/avatars/thumbnails/467/123/64/64/637748529243644133.png" alt="drawing" width="28"/> [Effective](https://www.curseforge.com/minecraft/mc-mods/effective) - добавляет эффекты взаимодействия с окружающей средой
- <img src="https://cdn.modrinth.com/data/WhbRG4iK/icon.gif" alt="drawing" width="28"/> [FallingLeaves](https://modrinth.com/mod/fallingleaves) - добавляет аккуратный эффект маленьких частиц к листовым блокам
- <img src="https://cdn.modrinth.com/data/YL57xq9U/icon.webp" alt="drawing" width="28"/> [Iris](https://modrinth.com/mod/iris) - шейдеры (Optifine формат)
- <img src="https://cdn.modrinth.com/data/2Uev7LdA/icon.png" alt="drawing" width="28"/> [LambdaBetterGrass](https://modrinth.com/mod/lambdabettergrass) - улучшает блоки травы и снега
- <img src="https://cdn.modrinth.com/data/yBW8D80W/icon.png" alt="drawing" width="28"/> [LambDynamicLights](https://modrinth.com/mod/lambdynamiclights) - динамическое освещение
- <img src="https://cdn.modrinth.com/data/MPCX6s5C/icon.png" alt="drawing" width="28"/> [NotEnoughAnimations](https://modrinth.com/mod/not-enough-animations) - перенос анимации от первого лица в вид от третьего лица
- <img src="https://cdn.modrinth.com/data/rcTfTZr3/icon.png" alt="drawing" width="28"/> [PresenceFootsteps](https://modrinth.com/mod/presence-footsteps) - добавляет больше звуков в игру

### Полезное:
- <img src="https://cdn.modrinth.com/data/EsAfCjCV/icon.png" alt="drawing" width="28"/> [AppleSkin](https://modrinth.com/mod/appleskin) - улучшение HUD, связанные с едой/голодом
- <img src="https://cdn.modrinth.com/data/MS1ZMyR7/icon.png" alt="drawing" width="28"/> [BetterPingDisplay](https://modrinth.com/mod/better-ping-display-fabric) - добавляет числовое отображение пинга
- <img src="https://cdn.modrinth.com/data/G1s2WpNo/icon.png" alt="drawing" width="28"/> [BetterThirdPerson](https://modrinth.com/mod/better-third-person) - улучшает вид камеры от третьего лица
- <img src="https://cdn.modrinth.com/data/89Wsn8GD/icon.png" alt="drawing" width="28"/> [Capes](https://modrinth.com/mod/capes) - отображение Optifine плащей
- <img src="https://cdn.modrinth.com/data/l94x8a1f/icon.png" alt="drawing" width="28"/> [FeaturedServers](https://modrinth.com/mod/featuredserversfabric) - избранные сервера
- <img src="https://cdn.modrinth.com/data/B3HO5V57/icon.png" alt="drawing" width="28"/> [ItemModelFix](https://modrinth.com/mod/item-model-fix) - исправляет пробелы в моделях предметов
- <img src="https://cdn.modrinth.com/data/mOgUt4GM/icon.png" alt="drawing" width="28"/> [ModMenu](https://modrinth.com/mod/modmenu) - меню с установленными модификациями для их просмотра и настройки
- <img src="https://cdn.modrinth.com/data/1KiJRrTg/icon.png" alt="drawing" width="28"/> [ScreenshotToClipboard](https://modrinth.com/mod/screenshot-to-clipboard) - сделанные скриншоты копируются в буфер обмена
- <img src="https://cdn.modrinth.com/data/w7ThoJFB/25d48c335340c12566044c8f35df5102e72dc06c.png" alt="drawing" width="28"/> [Zoomify](https://modrinth.com/mod/zoomify) - масштабирование по нажатию клавиши

### Оптимизация:
- <img src="https://cdn.modrinth.com/data/flmhXQgb/icon.png" alt="drawing" width="28"/> [ClientSideNoteblocks](https://modrinth.com/mod/clientsidenoteblocks) - устраняет лаги при воспроизведении нотных блоков
- <img src="https://cdn.modrinth.com/data/QwxR6Gcd/d1db8e74a5ad29908bd011ce271145cea349cd13.png" alt="drawing" width="28"/> [Debugify](https://modrinth.com/mod/debugify) - различные исправления багов
- <img src="https://cdn.modrinth.com/data/OVuFYfre/icon.png" alt="drawing" width="28"/> [EnhancedBlockEntities](https://modrinth.com/mod/ebe) - существенная оптимизация сундуков, табличек, кроватей, колоколов, шалкеров
- <img src="https://cdn.modrinth.com/data/NNAgCjsB/icon.png" alt="drawing" width="28"/> [EntityCulling](https://modrinth.com/mod/entityculling) - оптимизация сущностей
- <img src="https://cdn.modrinth.com/data/fQEb0iXm/icon.png" alt="drawing" width="28"/> [Krypton](https://modrinth.com/mod/krypton) - оптимизация сетевого стека
- <img src="https://cdn.modrinth.com/data/hvFnDODi/icon.png" alt="drawing" width="28"/> [LazyDFU](https://modrinth.com/mod/lazydfu) - оптимизация загрузки модов
- <img src="https://cdn.modrinth.com/data/gvQqBUqZ/icon.png" alt="drawing" width="28"/> [Lithium](https://modrinth.com/mod/lithium) - оптимизация Minecraft
- <img src="https://cdn.modrinth.com/data/NRjRiSSD/icon.png" alt="drawing" width="28"/> [MemoryLeakFix](https://modrinth.com/mod/memoryleakfix) - исправляет случайные утечки памяти
- <img src="https://cdn.modrinth.com/data/51shyZVL/icon.png" alt="drawing" width="28"/> [MoreCulling](https://modrinth.com/mod/moreculling) - оптимизация некоторых предметов
- <img src="https://cdn.modrinth.com/data/AFirzqTJ/icon.png" alt="drawing" width="28"/> [PurpurClient](https://modrinth.com/mod/purpurclient) - исправления ошибок ванили на стороне клиента
- <img src="https://cdn.modrinth.com/data/AANobbMI/icon.png" alt="drawing" width="28"/> [Sodium](https://modrinth.com/mod/sodium) - переписывание кода рендер-движка игры, значительно повышает производительность
  - <img src="https://cdn.modrinth.com/data/Orvt0mRa/icon.png" alt="drawing" width="28"/> [Sodium_Indium](https://modrinth.com/mod/indium) - нужен для совместимости Sodium с некоторыми модификации
  - <img src="https://cdn.modrinth.com/data/Bh37bMuy/icon.png" alt="drawing" width="28"/> [Sodium_ReesesSodiumOptions](https://modrinth.com/mod/reeses-sodium-options) - альтернативное меню для Sodium
  - <img src="https://cdn.modrinth.com/data/PtjYWJkn/icon.png" alt="drawing" width="28"/> [Sodium_SodiumExtra](https://modrinth.com/mod/sodium-extra) - больше настроек для Sodium
- <img src="https://cdn.modrinth.com/data/H8CaAYZC/icon.png" alt="drawing" width="28"/> [Starlight](https://modrinth.com/mod/starlight) - переписывание кода светового-движка игры, значительно повышает производительность

### Библиотеки:
- <img src="https://cdn.modrinth.com/data/9s6osm5g/icon.png" alt="drawing" width="28"/> [API_ClothConfig](https://modrinth.com/mod/cloth-config) - некоторые модификации используют его
- <img src="https://cdn.modrinth.com/data/P7dR8mSH/icon.png" alt="drawing" width="28"/> [API_FabricAPI](https://modrinth.com/mod/fabric-api) - базовая библиотека, нужна для большинства модификаций
- <img src="https://cdn.modrinth.com/data/Ha28R6CL/icon.png" alt="drawing" width="28"/> [API_FabricLanguageKotlin](https://modrinth.com/mod/fabric-language-kotlin) - некоторые модификации используют его

### Библиотеки ресурс паков:
- <img src="https://cdn.modrinth.com/data/otVJckYQ/icon.png" alt="drawing" width="28"/> [CITResewn](https://modrinth.com/mod/cit-resewn) - поддержка переименование предметов (Optifine формат)
- <img src="https://cdn.modrinth.com/data/1IjD5062/icon.png" alt="drawing" width="28"/> [Continuity](https://modrinth.com/mod/continuity) - поддержка соединения текстур (Optifine формат)
- <img src="https://cdn.modrinth.com/data/YOQCucah/icon.png" alt="drawing" width="28"/> [CustomEntityModels](https://modrinth.com/mod/cem) - поддержка моделей существ (Optifine формат)

